import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Scanner;

public class BrowserClient {
	public static void main(String[] args) throws UnknownHostException, IOException {
		Scanner sc = new Scanner(System.in);
		System.out.print("Web Page: ");
		String link = sc.nextLine();
		
		URL url = new URL(link);
		String host = url.getHost();
		String path = url.getPath();
		Socket connectTo = new Socket(host, 80);
		System.out.println(connectTo.getInetAddress().getHostAddress() + ":" + connectTo.getPort());
		
		BufferedReader in = new BufferedReader(new InputStreamReader(connectTo.getInputStream()));
        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(connectTo.getOutputStream()));
        
        out.write("GET " + path +" HTTP/1.1\r\n");
        out.write("Host: " + host + "\r\n");
        out.write("Connection: keep-alive\r\n");
        out.write("User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36\r\n");
        out.write("Accept: */*\r\n");
        out.write("Accept-Encoding: gzip, deflate, sdch\r\n");
        out.write("Accept-Language: en-US,en;q=0.8\r\n");
        out.write("\r\n");
        out.flush();
        
        String s;
        while ((s = in.readLine()) != null) {
            System.out.println(s);
            if (s.isEmpty()) {
                break;
            }
        }
        
        in.close();
        out.close();
        connectTo.close();
        sc.close();
	}
}
