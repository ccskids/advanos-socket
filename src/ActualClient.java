

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.Vector;


/**
 * 
 * @author Miko Garcia
 *
 * <PROTOCOL> <DESTINATION IP> <SOURCE IP> <MESSAGE> 
 *
 */
public class ActualClient extends Thread {
	public static String PROTOCOL_LOG = "LOGIN";
	public static String PROTOCOL_BROADCAST = "BROADCAST";
	public static String PROTOCOL_MESSAGE = "PM";
	public static String PROTOCOL_HTTP_REQUEST = "HTTP_REQUEST";
	public static String PROTOCOL_HTTP_RESPONSE = "HTTP_RESPONSE";
	
	private Socket server;
	private DataInputStream in;
	private DataOutputStream out;
	
	private Vector<String> sent_buffer;
	private String ip;
	
	public ActualClient() throws UnknownHostException, IOException {
		server = new Socket(RemoteServer.IP, RemoteServer.PORT);
		in = new DataInputStream(server.getInputStream());
		out = new DataOutputStream(server.getOutputStream());
		ip = InetAddress.getLocalHost().getHostAddress();
	}
	
	public ActualClient(String hostName) throws UnknownHostException, IOException {
		server = new Socket(RemoteServer.IP, RemoteServer.PORT);
		in = new DataInputStream(server.getInputStream());
		out = new DataOutputStream(server.getOutputStream());
		ip = InetAddress.getLocalHost().getHostAddress();
		
		send(buildMessage(PROTOCOL_LOG, RemoteServer.IP, ip, hostName));
	}
	
	public String buildMessage(String protocol, String destIP, String srcIP, String message) {
		return protocol + " " 
				+ destIP + " " 
				+ srcIP  + " " 
				+ message;
	}
	
	public String buildMessage(String protocol, String message) {
		return protocol + " " + message;
	}
 	
	public void send(String message) throws IOException {
		out.writeUTF(message);
		sent_buffer.add(message);
	}
	
	public boolean inBuffer(String message) {
		for(String s:sent_buffer) {
			if(message.equalsIgnoreCase(s)) {
				return true;
			}
		}
		
		return false;
	}
	
	public boolean removeFromBuffer(String msg) {
		String toRemove = null;
		for(String m: sent_buffer) {
			if(m.equalsIgnoreCase(msg)) {
				toRemove = m;
				break;
			}
		}
		
		if(toRemove != null) {
			sent_buffer.remove(toRemove);
			return true;
		} else {
			return false;
		}
	}
	
	public void run() {
		while(true) {
			try {
				String srvrMsg = in.readUTF();
				StringTokenizer st = new StringTokenizer(srvrMsg);
				String protocol = st.nextToken();
//				String destIP = st.nextToken();
//				String srcIP = st.nextToken();
				String message = "";
				
				while(st.hasMoreTokens()) {
					message += st.nextToken() + " ";
				} message.trim();
				
				//TO-DO Add the Protocols Here
				if(protocol.equalsIgnoreCase(PROTOCOL_HTTP_REQUEST)) {
					if(inBuffer(srvrMsg)) {
						removeFromBuffer(srvrMsg);
						//TO-DO Send requested page or info
					}
				} else if(protocol.equalsIgnoreCase(PROTOCOL_HTTP_RESPONSE)) {
					System.out.println(message);
				} else if(protocol.equalsIgnoreCase(PROTOCOL_BROADCAST)) {
					System.out.println(protocol + ": " + message);
				} else {
					System.out.println("PROTOCOL " + protocol + " not recognized");
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Username: ");
		String username = sc.nextLine();
		try {
			ActualClient client = new ActualClient(username);
			
			while(true) {
				System.out.print("Broadcast message: ");
				String message = sc.nextLine();
				
				String formatted = client.buildMessage(PROTOCOL_BROADCAST, message);
				client.send(formatted);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sc.close();
	}
}
