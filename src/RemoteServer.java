

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * 
 * @author Miko Garcia
 * Client will NOT have it's own socket because it doesn't receive
 * Client is in charge of sending out data
 * 
 * When a new socket is made, server.accept() will continue
 */

public class RemoteServer extends Thread{
	public static final int PORT = 5217;
	public static final String IP = "192.168.1.103";
	public static final String FILE_PATH = ""; 
	
	private ServerSocket srvr; 
	private Vector<Client> clients;					//Para siyang DNS Server, keeping hold of the addresses and their names
	
	public RemoteServer() throws IOException {
		srvr = new ServerSocket(PORT);
		clients = new Vector<>();
	}
	
	public void run() {
		while(true) {
			Socket rcvr;
			try {
				rcvr = srvr.accept();
				Client client = new Client(rcvr);
				clients.add(client);
				
				client.start();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public class Client extends Thread {
		Socket clientSocket;
		DataOutputStream out;
		DataInputStream in;
		String hostName;
		
		public Client(Socket clientSocket) throws IOException {
			this.clientSocket = clientSocket;
			out = new DataOutputStream(clientSocket.getOutputStream());
			in = new DataInputStream(clientSocket.getInputStream());
		}
		
		public void run() {
			while(true) {
				try {
					String msgFromClient = in.readUTF();
					StringTokenizer st = new StringTokenizer(msgFromClient);
					String protocol = st.nextToken();
					String destIP = st.nextToken();										//Can be the IP or the host name assigned to it
					
					
					//SEND THE MESSAGE TO IT'S RECEPIENT
					if(protocol.equals(ActualClient.PROTOCOL_LOG)) {
						this.hostName = st.nextToken();
						System.out.println(hostName + " Connected");
					} else if(protocol.equals(ActualClient.PROTOCOL_BROADCAST)) {
						for(Client c: clients) {
							if(c != this) {
								c.out.writeUTF(msgFromClient);
							}
						}
					} else if(protocol.equals(ActualClient.PROTOCOL_MESSAGE)) {
						for(Client c: clients) {
							if(c.clientSocket.getInetAddress().getHostAddress().equalsIgnoreCase(destIP) || c.hostName.equals(destIP)) {
								c.out.writeUTF(msgFromClient);
							}
						}
					} else {
						System.out.println(msgFromClient);
						System.out.println("Protocol Not recognized");
					}
					
				} catch (IOException e) {
					e.printStackTrace();
					clients.remove(this);
					try {
						clientSocket.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					
					break;
				}
			}
		}
 	}
	
	public static void main(String[] args) throws IOException {
		RemoteServer server = new RemoteServer();
		server.start();
	}
	
}
