

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 
 * @author Miko Garcia
 *
 * Server is the one that handles receiving messages from the sockets
 *
 */
public class BrowserServer extends Thread {
	public static final int PORT = 5217;
	public static final String IP = "192.168.1.103";
	public static final String FILE_PATH = ""; 
	
	private ServerSocket srvr; 
	
	public BrowserServer() throws IOException {
		srvr = new ServerSocket(PORT);
	}
	
	public void run() {
		while(true) {
			Socket rcvr;
			try {
				rcvr = srvr.accept();
				
				BufferedReader in = new BufferedReader(new InputStreamReader(rcvr.getInputStream()));
		        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(rcvr.getOutputStream()));
		        
		        String s;
		        while ((s = in.readLine()) != null) {
		            System.out.println(s);
		            if (s.isEmpty()) {
		                break;
		            }
		        }

		        out.write("HTTP/1.0 200 OK\r\n");
		        // Header
		        out.write("Date: Fri, 31 Dec 1999 23:59:59 GMT\r\n");
		        out.write("Server: Apache/0.8.4\r\n");
		        out.write("Content-Type: text/html\r\n");
		        out.write("Expires: Sat, 01 Jan 2000 00:59:59 GMT\r\n");
		        out.write("Last-modified: Fri, 09 Aug 1996 14:21:40 GMT\r\n");
		        out.write("\r\n");
		        
		        //content
		        out.write("<TITLE>Example</TITLE>");
		        out.write("<P>This is an example page.</P>");

		        out.close();
		        in.close();
		        rcvr.close();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) throws IOException {
		BrowserServer server = new BrowserServer();
		server.start();
	}
}
